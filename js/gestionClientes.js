function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //console.table(JSON.parse(request.responseText).value)
      procesarClientes(request.responseText);
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(clientes){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  var JSONClientes = JSON.parse(clientes);
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  //alert(JSONProductos.value[0].ProductName);

  for (var i = 0; i < JSONClientes.value.length; i++) {
    console.log(JSONClientes.value[i].ContactName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    var columnaCiudad = document.createElement("td");
    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    columnaCiudad.innerText = JSONClientes.value[i].City;

    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    imgBandera.classList.add("flag");
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);

  divTabla.appendChild(tabla);
}
